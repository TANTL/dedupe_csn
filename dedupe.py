import pandas as pd
from glob import glob
import os, argparse


def deduper(args):
	if not os.path.exists(args.output_folder):
		os.makedirs(args.output_folder)
	my_dict = {}
	my_dict['full_set'] = set()
	if args.sorted_file_list is None:
		sorted_file_list = sorted(glob('*.csv'))
		if len(sorted_file_list)==0:
			print("No '.csv' file discover, please check.")
			return 404
		print('Discovered {} ".csv" files with the following descending priority order:'.format(len(sorted_file_list)))
	else:
		sorted_file_list = args.sorted_file_list
		print('Recieved {} ".csv" files with the following descending priority order:'.format(len(sorted_file_list)))
	for i,file in enumerate(sorted_file_list):
		print("({:02d}) '{}'".format(i,file))
	for file in sorted_file_list:
		if os.path.exists(file):
			df = pd.read_csv(file,dtype=str)
		else:
			print("'{}' not found, please check.".format(file))
			return 404
		
		if 'csn' in df:
			my_dict[file] = set(df['csn'])
			my_dict['full_set'] = my_dict['full_set'] | my_dict[file]
		else:
			print("No 'csn' header found in '{}', please check.".format(file))
			return 404

	my_dict['left_over'] = my_dict['full_set']
	for file in sorted_file_list:
		my_dict['deduped_'+file] = my_dict['left_over'] & my_dict[file]
		my_dict['left_over'] = my_dict['left_over'] - my_dict[file]
		
	files_to_save = [_ for _ in my_dict if 'deduped' in _]
	for file in files_to_save:
		fname = os.path.join(args.output_folder,file)
		pd.DataFrame({'csn':list(my_dict[file])}).to_csv(fname,index=0)
		print("Saved to '{}' with".format(fname),end=' ')
		if my_dict[file.replace('deduped_','')] == my_dict[file]:
			print("'{}' preserving all entries.".format(file.replace('deduped_','')))
		else:
			print("'{}' dropping {} entries.".format(file,len(my_dict[file.replace('deduped_','')]-my_dict[file])))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description='Command line tool for de-duplicating CSV files.',
		formatter_class=argparse.RawTextHelpFormatter,
		)
	parser.add_argument('-OF', '--output-folder', type=str, action='store', dest='output_folder', default='output', 
		help='specify the output folder name (default:"output")')
	parser.add_argument('-PL', "--priority-list", nargs='+', type=str, dest='sorted_file_list', default=None,
		help="Provide a list of files to analyze",)
	deduper(parser.parse_args())